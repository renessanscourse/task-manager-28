package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;


public final class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_EXIT;
    }

    @NotNull
    @Override
    public String description() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}