package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectDTO;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_REMOVE_BY_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO projectDTO =
                endpointLocator.getProjectEndpoint().removeProjectByName(sessionDTO, name);
        if (projectDTO == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}