package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public class ServicePortCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SERVER_PORT;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server port";
    }

    @Override
    public void execute() {
        System.out.println("[PORT]");
        System.out.println(endpointLocator.getStorageEndpoint().getServerPortInfo());
        System.out.println("[OK]");
    }

}
