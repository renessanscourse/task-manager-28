package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.REGISTRY;
    }

    @NotNull
    @Override
    public String description() {
        return "Register new account";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().createWithLoginAndPassword(login, password);
        System.out.println("[OK]");
    }

}