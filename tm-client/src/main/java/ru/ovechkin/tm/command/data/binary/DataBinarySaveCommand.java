package ru.ovechkin.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;

import java.io.*;
import java.nio.file.Files;

public class DataBinarySaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BIN_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");
        endpointLocator.getStorageEndpoint().dataBinarySave(sessionDTO);
        System.out.println("[OK]");
    }

}