package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.Collection;

public final class ArgumentsAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String description() {
        return "Show available arguments";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.initCommands();
        @NotNull final Collection<AbstractCommand> arguments = bootstrap.getArguments();
        for (@NotNull final AbstractCommand argument : arguments) System.out.println(argument.arg());
    }

}