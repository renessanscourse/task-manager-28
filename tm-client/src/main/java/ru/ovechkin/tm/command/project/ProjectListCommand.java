package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.ProjectDTO;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_PROJECT_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @NotNull final List<ProjectDTO> projectsDTO =
                endpointLocator.getProjectEndpoint().findUserProjects(sessionDTO);
        int index = 1;
        for (final ProjectDTO projectDTO : projectsDTO) {
            System.out.println(index + ". " + projectDTO);
            index++;
        }
        System.out.println("[OK]");
    }

}