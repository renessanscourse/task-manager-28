package ru.ovechkin.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;

public class DataXmlJaxbLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_XML_JAXB_LOAD;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        endpointLocator.getStorageEndpoint().dataXmlJaxbLoad(sessionDTO);
        System.out.println("[OK]");
    }

}