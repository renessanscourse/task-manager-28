package ru.ovechkin.tm.api.locator;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.endpoint.*;

public interface IEndpointLocator {

    @NotNull UserEndpoint getUserEndpoint();

    @NotNull SessionEndpoint getSessionEndpoint();

    @NotNull StorageEndpoint getStorageEndpoint();

    @NotNull TaskEndpoint getTaskEndpoint();

    @NotNull ProjectEndpoint getProjectEndpoint();
}
