package ru.ovechkin.tm.exeption.empty;

public class MiddleNameEmptyException extends RuntimeException {

    public MiddleNameEmptyException() {
        super("Error! Middle name is empty...");
    }

}