package ru.ovechkin.tm.constant;

import org.jetbrains.annotations.NotNull;

@NotNull
public interface CmdConst {

    @NotNull
    String CMD_HELP = "help";

    @NotNull
    String CMD_VERSION = "version";

    @NotNull
    String CMD_ABOUT = "about";

    @NotNull
    String CMD_EXIT = "exit";

    @NotNull
    String CMD_INFO = "info";

    @NotNull
    String CMD_ARGUMENTS = "arguments";

    @NotNull
    String CMD_COMMANDS = "commands";

    @NotNull
    String CMD_TASK_LIST = "task-list";

    @NotNull
    String CMD_TASK_CLEAR = "task-clear";

    @NotNull
    String CMD_TASK_CREATE = "task-create";

    @NotNull
    String CMD_PROJECT_LIST = "project-list";

    @NotNull
    String CMD_PROJECT_CLEAR = "project-clear";

    @NotNull
    String CMD_PROJECT_CREATE = "project-create";

    @NotNull
    String TASK_SHOW_BY_ID = "task-show-by-id";

    @NotNull
    String TASK_SHOW_BY_INDEX = "task-show-by-index";

    @NotNull
    String TASK_SHOW_BY_NAME = "task-show-by-name";

    @NotNull
    String TASK_UPDATE_BY_ID = "task-update-by-id";

    @NotNull
    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    @NotNull
    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    @NotNull
    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    @NotNull
    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    @NotNull
    String PROJECT_SHOW_BY_ID = "project-show-by-id";

    @NotNull
    String PROJECT_SHOW_BY_INDEX = "project-show-by-index";

    @NotNull
    String PROJECT_SHOW_BY_NAME = "project-show-by-name";

    @NotNull
    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    @NotNull
    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    @NotNull
    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    @NotNull
    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    @NotNull
    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    @NotNull
    String LOGIN = "login";

    @NotNull
    String LOGOUT = "logout";

    @NotNull
    String REGISTRY = "registry";

    @NotNull
    String SHOW_PROFILE = "show-profile";

    @NotNull
    String UPDATE_PROFILE = "update-profile";

    @NotNull
    String UPDATE_PASSWORD = "update-password";

    @NotNull
    String DATA_BIN_SAVE = "data-bin-save";

    @NotNull
    String DATA_BIN_LOAD = "data-bin-load";

    @NotNull
    String DATA_BASE64_SAVE = "data-base64-save";

    @NotNull
    String DATA_BASE64_LOAD = "data-base64-load";

    @NotNull
    String DATA_JSON_JAXB_SAVE = "data-json-jaxb-save";

    @NotNull
    String DATA_JSON_JAXB_LOAD = "data-json-jaxb-load";

    @NotNull
    String DATA_XML_JAXB_SAVE = "data-xml-jaxb-save";

    @NotNull
    String DATA_XML_JAXB_LOAD = "data-xml-jaxb-load";

    @NotNull
    String DATA_JSON_MAPPER_SAVE = "data-json-mapper-save";

    @NotNull
    String DATA_JSON_MAPPER_LOAD = "data-json-mapper-load";

    @NotNull
    String DATA_XML_MAPPER_SAVE = "data-xml-mapper-save";

    @NotNull
    String DATA_XML_MAPPER_LOAD = "data-xml-mapper-load";

    @NotNull
    String SERVER_HOST = "show-host";

    @NotNull
    String SERVER_PORT = "show-port";

}