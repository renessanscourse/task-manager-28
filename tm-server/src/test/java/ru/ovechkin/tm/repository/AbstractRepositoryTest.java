package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Абстрактный репозиторий тестируется на основании проектного репозитория,
 * но можно выбрать любой другой репозиторий, поскольку каждый из них
 * наследует абстрактный
 */

public class AbstractRepositoryTest {
/*
    private final AbstractRepository<Project> abstractRepository = new ProjectRepository();

    @Before
    public void clearRepository() {
        abstractRepository.clear();
    }

    @Test
    public void testClear() {
        abstractRepository.merge(listOfTestProjects());
        abstractRepository.clear();
        Assert.assertTrue(abstractRepository.findAll().isEmpty());
    }

    @Test
    public void testFindAll() {
        Assert.assertNotNull(abstractRepository.findAll());
        abstractRepository.merge(listOfTestProjects());
        Assert.assertEquals(listOfTestProjects(), abstractRepository.findAll());
    }

    @Test
    public void testMergeOneEntity() {
        abstractRepository.merge(projectDTO1);
        Assert.assertTrue(abstractRepository.findAll().contains(projectDTO1));
    }

    @Test
    public void testMergeListOfEntities() {
        abstractRepository.merge(listOfTestProjects());
        Assert.assertEquals(listOfTestProjects(), abstractRepository.findAll());
    }

    @Test
    public void testManyOfEntities() {
        abstractRepository.merge(projectDTO1, projectDTO2, projectDTO3);
        Assert.assertTrue(abstractRepository.findAll().contains(projectDTO1));
        Assert.assertTrue(abstractRepository.findAll().contains(projectDTO2));
        Assert.assertTrue(abstractRepository.findAll().contains(projectDTO3));
    }

    @Test
    public void testLoadListOfEntities() {
        abstractRepository.load(listOfTestProjects());
        Assert.assertEquals(listOfTestProjects(), abstractRepository.findAll());
    }

    @Test
    public void testLoadManyOfEntities() {
        abstractRepository.load(projectDTO1, projectDTO2, projectDTO3);
        final List<ProjectDTO> testProjectDTOList = new ArrayList<>();
        testProjectDTOList.add(projectDTO1);
        testProjectDTOList.add(projectDTO2);
        testProjectDTOList.add(projectDTO3);
        Assert.assertEquals(testProjectDTOList, abstractRepository.findAll());
    }
*/
}