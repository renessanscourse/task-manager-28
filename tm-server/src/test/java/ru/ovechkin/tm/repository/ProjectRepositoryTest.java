package ru.ovechkin.tm.repository;

import org.junit.*;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.service.IEntityManagerService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.locator.ServiceLocator;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepositoryTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();

    {
        try {
            entityManagerService.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final EntityManager entityManager = entityManagerService.getEntityManager();

    private final IProjectRepository projectRepository = new ProjectRepository(entityManager);

    @Test
    public void testAdd() {
        final Project project = new Project();
        project.setName("testProject");
        projectRepository.add(project);
    }

    @Test
    public void testGetList() {
        final List<Project> projects = projectRepository.findUserProjects("22bd73ee-90f3-4cde-b4ca-182397d3adfa");
        System.out.println(projects.toString());
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void testFindProjectById() {
        System.out.println(projectRepository.findById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adf",
                "2425d33f-e71d-483f-a28c-876d394180f2"));
    }

    @Test
    public void testRemoveAll() {
        projectRepository.removeAll("22bd73ee-90f3-4cde-b4ca-182397d3adfa");
    }

    @Test
    public void testRemoveByName() {
        projectRepository.removeByName(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "testProjectDTO1"
        );
    }

    @Test
    public void testRemoveById() {
        projectRepository.removeById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "8ec61bc3-ba65-430a-9b92-987224646f6b"
        );
    }
/*
    private final IProjectRepository projectRepository = new ProjectRepository();

    private  final ProjectsForTest projectsForTest = new ProjectsForTest();

    private  final ProjectDTO projectDTO1 = projectsForTest.getProject1();

    private  final ProjectDTO projectDTO2 = projectsForTest.getProject2();

    private final ProjectDTO projectDTO3 = projectsForTest.getProject3();

    public List<ProjectDTO> listOfTestProjects() {
        return projectsForTest.getListOfTestProjects();
    }

    @Before
    public void clearRepository() {
        projectRepository.clear();
        projectRepository.merge(listOfTestProjects());
    }

    @Test
    public void testAdd() {
        projectRepository.clear();
        projectRepository.add(projectDTO1.getUserId(), projectDTO1);
        Assert.assertEquals(projectDTO1, projectRepository.findById(projectDTO1.getUserId(), projectDTO1.getId()));
    }

    @Test
    public void testFindUserProjects() {
        final List<ProjectDTO> projectsOfUsers = listOfTestProjects();
        projectsOfUsers.remove(projectDTO3);// убираем проект с ненужным userId
        Assert.assertEquals(projectsOfUsers, projectRepository.findUserProjects(TestConst.USER_ID_USER));
    }

    @Test
    public void testRemoveAll() {
        projectRepository.merge(listOfTestProjects());
        final List<ProjectDTO> projectDTOS = listOfTestProjects();
        projectDTOS.removeAll(listOfTestProjects());
        projectRepository.removeAll(TestConst.USER_ID_USER);
        Assert.assertEquals(projectDTOS, projectRepository.findUserProjects(TestConst.USER_ID_USER));
    }

    @Test
    public void testFindById() {
        projectRepository.clear();
        projectRepository.merge(projectDTO1);
        Assert.assertEquals(projectDTO1, projectRepository.findById(projectDTO1.getUserId(), projectDTO1.getId()));
        Assert.assertNull(projectRepository.findById(projectDTO2.getUserId(), projectDTO2.getId()));
        Assert.assertNull(projectRepository.findById(projectDTO2.getUserId(), projectDTO3.getId()));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(projectDTO3, projectRepository.findByIndex(projectDTO3.getUserId(), 1));
        Assert.assertNull(projectRepository.findByIndex(projectDTO3.getUserId(), 2));
    }

    @Test
    public void testFindByName() {
        Assert.assertEquals(projectDTO3, projectRepository.findByName(projectDTO3.getUserId(), projectDTO3.getName()));
        Assert.assertNull(projectRepository.findByName(projectDTO3.getUserId(), "project3.getName())"));
        Assert.assertNull(projectRepository.findByName(projectDTO2.getUserId(), projectDTO3.getName()));
    }

    @Test
    public void testRemoveById() {
        projectRepository.removeById(projectDTO1.getUserId(), projectDTO1.getId());
        Assert.assertNull(projectRepository.findById(projectDTO1.getUserId(), projectDTO1.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        projectRepository.removeByIndex(projectDTO1.getUserId(), 1);
        Assert.assertNull(projectRepository.findById(projectDTO1.getUserId(), projectDTO1.getId()));
    }

    @Test
    public void testRemoveByName() {
        projectRepository.removeByName(projectDTO1.getUserId(), projectDTO1.getName());
        Assert.assertNull(projectRepository.findByName(projectDTO1.getUserId(), projectDTO1.getName()));
    }
*/
}