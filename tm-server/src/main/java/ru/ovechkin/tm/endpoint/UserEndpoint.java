package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.IUserEndpoint;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
        userService = serviceLocator.getUserService();
        sessionService = serviceLocator.getSessionService();
    }

    @Nullable
    @WebMethod
    public UserDTO findById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.findById(id);
    }

    @Nullable
    @WebMethod
    public UserDTO findByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.findByLogin(login);
    }

    @Nullable
    @WebMethod
    public UserDTO createWithLoginAndPassword(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        return userService.create(login, password);
    }

    @Nullable
    @WebMethod
    public UserDTO createWithLoginPasswordAndRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role")final Role role
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.create(login, password, role);
    }

    @Nullable
    @WebMethod
    public UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.lockUserByLogin(sessionDTO.getUserId(), login);
    }

    @Nullable
    @WebMethod
    public UserDTO unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.unLockUserByLogin(sessionDTO.getUserId(), login);
    }

    @Nullable
    @WebMethod
    public UserDTO removeById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.removeById(sessionDTO.getUserId(), id);
    }

    @Nullable
    @WebMethod
    public UserDTO removeByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(sessionDTO, Role.ADMIN);
        return userService.removeByLogin(sessionDTO.getUserId(), login);
    }

    @Override
    @WebMethod
    public void updateProfileLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "newLogin", partName = "newLogin") final String newLogin
    ) {
        sessionService.validate(sessionDTO);
        userService.updateProfileLogin(sessionDTO.getUserId(), newLogin);
    }

    @Override
    @WebMethod
    public void updatePassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "currentPassword", partName = "currentPassword") final String currentPassword,
            @Nullable @WebParam(name = "newPassword", partName = "newEmail") final String newPassword
    ) {
        sessionService.validate(sessionDTO);
        userService.updatePassword(sessionDTO.getUserId(), currentPassword, newPassword);
    }

}