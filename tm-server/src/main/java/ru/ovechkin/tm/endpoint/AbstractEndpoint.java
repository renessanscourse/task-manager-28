package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.enumirated.Role;

public abstract class AbstractEndpoint {

    @Nullable
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@Nullable IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles() {
        return null;
    }

}