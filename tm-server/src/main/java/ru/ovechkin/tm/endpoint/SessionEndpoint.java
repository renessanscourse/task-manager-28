package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.ISessionEndpoint;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.dto.*;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) throws AccessForbiddenException {
        return serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(sessionDTO);
        try {
            serviceLocator.getSessionService().close(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public List<SessionDTO> sessionsOfUser(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getSessionService().getListSession(sessionDTO);
    }

    @Override
    public boolean isValid(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getSessionService().isValid(sessionDTO);
    }

    @Override
    public void signOutByLogin(@Nullable @WebParam(name = "login", partName = "login") String login) {
        serviceLocator.getSessionService().signOutByLogin(login);
    }

    @Override
    public void signOutByUserId(@Nullable @WebParam(name = "userId", partName = "userId") String userId) {
        serviceLocator.getSessionService().signOutByUserId(userId);
    }

    @Override
    public @NotNull UserDTO getUser(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getSessionService().getUser(sessionDTO);
    }

    @Override
    public void closeAll(@Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO) throws AccessForbiddenException {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getSessionService().closeAll(sessionDTO);
    }

}