package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.endpoint.IProjectEndpoint;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    private ISessionService sessionService;

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(final IServiceLocator IServiceLocator) {
        super(IServiceLocator);
        this.projectService = IServiceLocator.getProjectService();
        this.sessionService = IServiceLocator.getSessionService();
    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "project", partName = "project") ProjectDTO project
    ) {
        sessionService.validate(sessionDTO);
        projectService.add(sessionDTO, project);
    }

    @Override
    @WebMethod
    public void createProjectWithName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(sessionDTO);
        projectService.create(sessionDTO, name);
    }

    @Override
    @WebMethod
    public void createProjectWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(sessionDTO);
        projectService.create(sessionDTO, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public List<ProjectDTO> findUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        sessionService.validate(sessionDTO);
        return projectService.findUserProjects(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        sessionService.validate(sessionDTO);
        projectService.removeAllUserProjects(sessionDTO.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(sessionDTO);
        return projectService.findProjectById(sessionDTO.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(sessionDTO);
        return projectService.findProjectByName(sessionDTO.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(sessionDTO);
        return projectService.updateProjectById(sessionDTO.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(sessionDTO);
        return projectService.removeProjectById(sessionDTO.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(sessionDTO);
        return projectService.removeProjectByName(sessionDTO.getUserId(), name);
    }

}