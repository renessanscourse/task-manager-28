package ru.ovechkin.tm.exeption.empty;

public class UsersListEmptyException extends RuntimeException {

    public UsersListEmptyException() {
        super("Error! There is no users in database to save...");
    }

}