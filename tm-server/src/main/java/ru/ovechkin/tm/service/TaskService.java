package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.service.IEntityManagerService;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.TaskDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.exeption.other.NameAlreadyTakenException;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.unknown.TaskUnknownException;
import ru.ovechkin.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

public class TaskService implements ITaskService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public TaskService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void add(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final TaskDTO taskDTO,
            @Nullable final ProjectDTO projectDTO
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (taskDTO == null) return;
        if (projectDTO == null) return;
        @NotNull final UserDTO userDTO = serviceLocator.getSessionService().getUser(sessionDTO);
        @NotNull final User user = new User(userDTO);
        @NotNull final Task task = new Task(taskDTO);
        @NotNull final Project project = new Project(projectDTO);
        task.setUser(user);
        task.setProject(project);
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        if (taskRepository.findByName(sessionDTO.getUserId(), task.getName()) != null)
            throw new NameAlreadyTakenException(task.getName());
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            taskRepository.add(task);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String taskName,
            @Nullable final String projectId
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(taskName);
        @NotNull final ProjectDTO projectDTO =
                serviceLocator.getProjectService().findProjectById(sessionDTO.getUserId(), projectId);
        add(sessionDTO, taskDTO, projectDTO);
    }

    @Override
    public void create(
            @Nullable final SessionDTO sessionDTO,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String projectId
    ) {
        if (sessionDTO == null) throw new NotLoggedInException();
        if (taskName == null || taskName.isEmpty()) throw new NameEmptyException();
        if (taskDescription == null || taskDescription.isEmpty()) throw new DescriptionEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(taskName);
        taskDTO.setDescription(taskDescription);
        @NotNull final ProjectDTO projectDTO =
                serviceLocator.getProjectService().findProjectById(sessionDTO.getUserId(), projectId);
        add(sessionDTO, taskDTO, projectDTO);
    }

    @NotNull
    @Override
    public List<TaskDTO> findUserTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Task> tasks = taskRepository.findUserTasks(userId);
        entityManager.close();
        if (tasks == null || tasks.isEmpty()) throw new ProjectListEmptyException();
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            @NotNull final TaskDTO taskDTO = new TaskDTO(task);
            tasksDTO.add(taskDTO);
        }
        return tasksDTO;
    }

    @Override
    public void removeAllUserTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            taskRepository.removeAll(userId);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findTaskById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final Task task = taskRepository.findById(userId, id);
        entityManager.close();
        if (task == null) throw new TaskUnknownException();
        @NotNull final TaskDTO taskDTO = new TaskDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    public TaskDTO findTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final Task task = taskRepository.findByName(userId, name);
        entityManager.close();
        if (task == null) throw new TaskUnknownException(name);
        @NotNull final TaskDTO taskDTO = new TaskDTO(task);
        return taskDTO;
    }

    @NotNull
    @Override
    public TaskDTO updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        if (taskRepository.findByName(userId, name) != null)
            throw new NameAlreadyTakenException(name);
        @Nullable final Task task = taskRepository.findById(userId, id);
        if (task == null) throw new TaskUnknownException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(task);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        @NotNull final TaskDTO taskDTO = new TaskDTO(task);
        return taskDTO;
    }

    @Nullable
    @Override
    public TaskDTO removeTaskById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            @Nullable final Task task = taskRepository.removeById(userId, id);
            if (task == null) throw new TaskUnknownException();
            @NotNull final TaskDTO taskDTO = new TaskDTO(task);
            transaction.commit();
            return taskDTO;
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO removeTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new IdEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            @Nullable final Task task = taskRepository.removeByName(userId, name);
            if (task == null) throw new TaskUnknownException();
            @NotNull final TaskDTO taskDTO = new TaskDTO(task);
            transaction.commit();
            return taskDTO;
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> getAllTasksDTO() {
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<Task> taskList = taskRepository.getAllTasks();
        entityManager.close();
        if (taskList == null || taskList.isEmpty()) throw new TaskUnknownException();
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            @NotNull final TaskDTO taskDTO = new TaskDTO(task);
            tasksDTO.add(taskDTO);
        }
        return tasksDTO;
    }

    @NotNull
    @Override
    public List<TaskDTO> loadTasks(@Nullable final List<TaskDTO> tasksDTO) {
        if (tasksDTO == null || tasksDTO.isEmpty()) throw new TaskUnknownException();
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.close();
        for (@NotNull final TaskDTO taskDTO : tasksDTO) {
            @NotNull final Task task = new Task(taskDTO);
            taskList.add(task);
        }
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            taskRepository.mergeCollection(taskList);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return tasksDTO;
    }

    @Override
    public void removeAllTasks() {
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            taskRepository.removeAllTasks();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}