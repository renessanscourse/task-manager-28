package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.IEntityManagerService;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.other.SomethingWentWrongException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.other.WrongCurrentPasswordException;
import ru.ovechkin.tm.exeption.user.*;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.ArrayList;
import java.util.List;

public class UserService implements IUserService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    public UserService(
            @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        final User user = userRepository.findById(id);
        entityManager.close();
        if (user == null) throw new UserDoesNotExistException();
        return new UserDTO(user);
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        try {
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) throw new UserDoesNotExistException();
            return new UserDTO(user);
        } catch (Exception e) {
            entityManager.close();
            throw e;
        }
    }

    @NotNull
    @Override
    public UserDTO removeUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) throw new UserEmptyException();
        final User user = new User(userDTO);
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            userRepository.removeUser(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public UserDTO removeById(@Nullable final String adminId, @Nullable final String id) {
        if (adminId == null || adminId.isEmpty()) throw new IdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO userDTO = findById(id);
        if (userDTO == null) throw new UserDoesNotExistException();
        final UserDTO currentUserDTO = findById(adminId);
        if (userDTO.getId().equals(currentUserDTO.getId())) throw new SelfRemovingException();
        if (userDTO.getRole() == Role.ADMIN) throw new AdminRemovingException();
        if (currentUserDTO.getRole() != Role.ADMIN) throw new AccessDeniedException();
        final User user = new User(userDTO);

        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            userRepository.removeById(user.getId());
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String userId, @Nullable final String login) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserDoesNotExistException();
        final UserDTO currentUserDTO = findById(userId);
        if (userDTO.getLogin().equals(currentUserDTO.getLogin())) throw new SelfRemovingException();
        if (userDTO.getRole() == Role.ADMIN) throw new AdminRemovingException();
        if (currentUserDTO.getRole() != Role.ADMIN) throw new AccessDeniedException();
        final User user = new User(userDTO);

        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            userRepository.removeByLogin(user.getLogin());
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USER);

        @NotNull final UserDTO userDTO = new UserDTO(user);

        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            userRepository.add(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);

        @NotNull final UserDTO userDTO = new UserDTO(user);

        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            userRepository.add(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public UserDTO lockUserByLogin(@Nullable final String userId, @Nullable final String login) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserDoesNotExistException();
        final UserDTO currentUserDTO = findById(userId);
        if (currentUserDTO == null) throw new UserDoesNotExistException();
        if (userDTO.getLogin().equals(currentUserDTO.getLogin())) throw new SelfBlockingException();
        if (userDTO.getRole() == Role.ADMIN) throw new AdminBlockingException();
        if (currentUserDTO.getRole() != Role.ADMIN) throw new AccessDeniedException();

        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();

        @NotNull final User user = new User(userDTO);
        user.setLocked(true);
        userDTO.setLocked(true);

        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public UserDTO unLockUserByLogin(@Nullable final String userId, @Nullable final String login) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final UserDTO userDTO = findByLogin(login);
        if (userDTO == null) throw new UserDoesNotExistException();
        final UserDTO currentUserDTO = findById(userId);
        if (currentUserDTO == null) throw new AccessDeniedException();
        if (currentUserDTO.getRole() != Role.ADMIN) throw new AccessDeniedException();

        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();

        @NotNull final User user = new User(userDTO);
        user.setLocked(false);
        userDTO.setLocked(false);

        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @NotNull
    @Override
    public List<UserDTO> getAllUsersDTO() {
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        @Nullable final List<User> userList = userRepository.getAllUsers();
        if (userList == null || userList.isEmpty()) throw new UsersListEmptyException();
        @NotNull final List<UserDTO> usersDTO = new ArrayList<>();
        try {
            transaction.begin();
            for (@NotNull final User user : userList) {
                @NotNull final UserDTO userDTO = new UserDTO(user);
                usersDTO.add(userDTO);
            }
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return usersDTO;
    }

    @NotNull
    @Override
    public List<UserDTO> loadUsers(@Nullable final List<UserDTO> usersDTO) {
        if (usersDTO == null || usersDTO.isEmpty()) throw new UsersListEmptyException();
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        for (@NotNull final UserDTO userDTO : usersDTO) {
            @NotNull final User user = new User(userDTO);
            userList.add(user);
        }
        try {
            transaction.begin();
            userRepository.mergeCollection(userList);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return usersDTO;
    }

    @Override
    public void removeAllUsers() {
        @NotNull final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            userRepository.removeAllUsers();
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public UserDTO updateProfileLogin(
            @Nullable final String userId,
            @Nullable final String newLogin
    ) {
        if (userId == null || userId.isEmpty()) throw new NotLoggedInException();
        if (newLogin == null || newLogin.isEmpty()) throw new LoginEmptyException();
        try {
            findByLogin(newLogin);
        } catch (UserDoesNotExistException e) {
            System.out.println("This login [" + newLogin + "] is free");
        }
        @Nullable final UserDTO userDTO = findById(userId);
        if (userDTO == null) throw new UserEmptyException();
        userDTO.setLogin(newLogin);
        @NotNull final User user = new User(userDTO);
        final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        final EntityManager entityManager = entityManagerService.getEntityManager();
        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return userDTO;
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String currentPassword,
            @Nullable final String newPassword
    ) {
        if (userId == null || userId.isEmpty()) throw new NotLoggedInException();
        if (currentPassword == null || currentPassword.isEmpty()) throw new PasswordEmptyException();
        if (newPassword == null || newPassword.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO userDTO = findById(userId);
        if (userDTO == null) throw new UserDoesNotExistException();

        @Nullable final String currentPasswordCheckHash = HashUtil.salt(currentPassword);
        if (currentPasswordCheckHash == null || currentPasswordCheckHash.isEmpty())
            throw new SomethingWentWrongException();
        @NotNull final String currentUserPasswordHash = userDTO.getPasswordHash();
        if (!currentPasswordCheckHash.equals(currentUserPasswordHash))
            throw new WrongCurrentPasswordException();

        @Nullable final String newPasswordHash = HashUtil.salt(newPassword);
        userDTO.setPasswordHash(newPasswordHash);

        @NotNull final User user = new User(userDTO);
        final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();
        entityManagerService.init();
        final EntityManager entityManager = entityManagerService.getEntityManager();
        final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(user);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}