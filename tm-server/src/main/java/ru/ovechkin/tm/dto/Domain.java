package ru.ovechkin.tm.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public final class Domain implements Serializable {

    private List<ProjectDTO> projectDTOS = new ArrayList<>();

    private List<TaskDTO> taskDTOS = new ArrayList<>();

    private List<UserDTO> userDTOS = new ArrayList<>();

    public List<ProjectDTO> getProjects() {
        return projectDTOS;
    }

    public void setProjects(List<ProjectDTO> projectDTOS) {
        this.projectDTOS = projectDTOS;
    }

    public List<TaskDTO> getTasks() {
        return taskDTOS;
    }

    public void setTasks(List<TaskDTO> taskDTOS) {
        this.taskDTOS = taskDTOS;
    }

    public List<UserDTO> getUsers() {
        return userDTOS;
    }

    public void setUsers(List<UserDTO> userDTOS) {
        this.userDTOS = userDTOS;
    }

}